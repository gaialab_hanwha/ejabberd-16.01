%%%----------------------------------------------------------------------
%%% File    : odbc_queries.erl
%%% Author  : Mickael Remond <mremond@process-one.net>
%%% Purpose : ODBC queries dependind on back-end
%%% Created : by Mickael Remond <mremond@process-one.net>
%%%
%%%
%%% ejabberd, Copyright (C) 2002-2015   ProcessOne
%%%
%%% This program is free software; you can redistribute it and/or
%%% modify it under the terms of the GNU General Public License as
%%% published by the Free Software Foundation; either version 2 of the
%%% License, or (at your option) any later version.
%%%
%%% This program is distributed in the hope that it will be useful,
%%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%%% General Public License for more details.
%%%
%%% You should have received a copy of the GNU General Public License along
%%% with this program; if not, write to the Free Software Foundation, Inc.,
%%% 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
%%%
%%%----------------------------------------------------------------------

-module(odbc_queries).

-behaviour(ejabberd_config).

-author("mremond@process-one.net").

-export([get_db_type/0, update/5, update_t/4,
	 sql_transaction/2, get_last/2, set_last_t/4, del_last/2,
	 get_password/2, get_password_scram/2, set_password_t/3,
	 set_password_scram_t/6, add_user/3, add_user_scram/6,
	 del_user/2, del_user_return_password/3, list_users/1,
	 list_users/2, users_number/1, users_number/2,
	 add_spool_sql/2, add_spool/2, get_and_del_spool_msg_t/2,
	 del_spool_msg/2, get_roster/2, get_roster_jid_groups/2,
	 get_roster_groups/3, del_user_roster_t/2,
	 get_roster_by_jid/3, get_rostergroup_by_jid/3,
	 del_roster/3, del_roster_sql/2, update_roster/5,
	 update_roster_sql/4, roster_subscribe/4,
	 get_subscription/3, set_private_data/4,
	 set_private_data_sql/3, get_private_data/3,
	 get_private_data/2, del_user_private_storage/2,
	 get_default_privacy_list/2,
	 get_default_privacy_list_t/1, get_privacy_list_names/2,
	 get_privacy_list_names_t/1, get_privacy_list_id/3,
	 get_privacy_list_id_t/2, get_privacy_list_data/3,
	 get_privacy_list_data_by_id/2,
	 get_privacy_list_data_t/2,
	 get_privacy_list_data_by_id_t/1,
	 set_default_privacy_list/2,
	 unset_default_privacy_list/2, remove_privacy_list/2,
	 add_privacy_list/2, set_privacy_list/2,
	 del_privacy_lists/3, set_vcard/26, get_vcard/2,
	 escape/1, count_records_where/3, get_roster_version/2,
	 set_roster_version/2, opt_type/1]).

-include("ejabberd.hrl").
-include("logger.hrl").

%% Almost a copy of string:join/2.
%% We use this version because string:join/2 is relatively
%% new function (introduced in R12B-0).
join([], _Sep) -> [];
join([H | T], Sep) -> [H, [[Sep, X] || X <- T]].

get_db_type() -> generic.

%% Safe atomic update.
update_t(Table, Fields, Vals, Where) ->
    UPairs = lists:zipwith(fun (A, B) ->
				   <<A/binary, "='", B/binary, "'">>
			   end,
			   Fields, Vals),
    case ejabberd_odbc:sql_query_t([<<"update ">>, Table,
				    <<" set ">>, join(UPairs, <<", ">>),
				    <<" where ">>, Where, <<";">>])
	of
      {updated, 1} -> ok;
      _ ->
		Res = ejabberd_odbc:sql_query_t([<<"insert into ">>, Table,
				     <<"(">>, join(Fields, <<", ">>),
				     <<") values ('">>, join(Vals, <<"', '">>),
				     <<"');">>]),
		case Res of
			{updated,1} -> ok;
			_ -> Res
		end
    end.

update_t2(Table, Fields, Vals, Where) ->
    UPairs = lists:zipwith(fun (A, B) ->
				   <<A/binary, "=", B/binary>>
			   end,
			   tl(Fields), tl(Vals)),
    case ejabberd_odbc:sql_query_t([<<"update ">>, Table,
				    <<" set ">>, join(UPairs, <<", ">>),
				    <<" where ">>, Where, <<";">>])
	of
      {updated, 1} -> ok;
      _ ->
		Res = ejabberd_odbc:sql_query_t([<<"insert into ">>, Table,
				     <<"(">>, join(Fields, <<", ">>),
				     <<") values (">>, join(Vals, <<", ">>),
				     <<");">>]),

		case Res of
			{updated,1} -> ok;
			_ -> Res
		end
    end.

update(LServer, Table, Fields, Vals, Where) ->
    UPairs = lists:zipwith(fun (A, B) ->
				   <<A/binary, "='", B/binary, "'">>
			   end,
			   Fields, Vals),
    case ejabberd_odbc:sql_query(LServer,
				 [<<"update ">>, Table, <<" set ">>,
				  join(UPairs, <<", ">>), <<" where ">>, Where,
				  <<";">>])
	of
      {updated, 1} -> ok;
      _ ->
		Res = ejabberd_odbc:sql_query(LServer,
				  [<<"insert into ">>, Table, <<"(">>,
				   join(Fields, <<", ">>), <<") values ('">>,
				   join(Vals, <<"', '">>), <<"');">>]),
		case Res of
			{updated,1} -> ok;
			_ -> Res
		end		   
    end.

%% F can be either a fun or a list of queries
%% TODO: We should probably move the list of queries transaction
%% wrapper from the ejabberd_odbc module to this one (odbc_queries)
sql_transaction(LServer, F) ->
    ejabberd_odbc:sql_transaction(LServer, F).

get_last(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select seconds, state from last where "
			       "username='">>,
			     Username, <<"'">>]).

set_last_t(LServer, Username, Seconds, State) ->
    update(LServer, <<"last">>,
	   [<<"username">>, <<"seconds">>, <<"state">>],
	   [Username, Seconds, State],
	   [<<"username='">>, Username, <<"'">>]).

del_last(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from last where username='">>, Username,
			     <<"'">>]).

hex2num(C) when C >= 16#30, C =< 16#39 -> C - 16#30; % 0-9
hex2num(C) when C >= 16#61, C =< 16#66 -> C - 16#61 + 10; % a-f
hex2num(C) when C >= 16#41, C =< 16#46 -> C - 16#41 + 10. % A-F

to_binary(H) ->
    list_to_binary([ hex2num(A) * 16 + hex2num(B) || <<A:8/integer, B:8/integer>> <= H ]).

get_password(LServer, Username) ->
    %ejabberd_odbc:sql_query(LServer,
	%		    [<<"select password from users where username='">>,
	%		     Username, <<"';">>]).
    Temp = ejabberd_odbc:sql_query(LServer,
			    [<<"select encryptedPassword from ofUser where username='">>,
			     Username, <<"';">>]),
		?DEBUG("sql_query ~p ~n", [Temp]),
		{selected, [<<"encryptedPassword">>], NCr} = Temp,
case NCr of
		[[Cr]] ->
%%[minchul] Check X Pending - MSSQL
			Cp = << <<X>> || <<X, 0>> <= Cr >>,
%%[minchul] You should do in mysql
%%			Cp = Cr,
			C = to_binary(Cp),
			%Key = <<"aU4a4RT8fsFmOhm">>,
			%K = crypto:sha(Key),
			K = <<252,197,110,102,36,15,0,226,223,0,7,204,100,131,113,58,112,92,54,231>>,
			<<IV:8/binary, Text/binary>> = C,
			P = crypto:blowfish_cbc_decrypt(K, IV, Text),
			{selected, [<<"password">>], [[ << <<X>> || <<0, X>> <= P >> ]]};
		[] ->
			{selected, [<<"password">>], []}
	end.

get_password_scram(LServer, Username) ->
    ejabberd_odbc:sql_query(
      LServer,
      [<<"select password, serverkey, salt, iterationcount from users where "
        "username='">>, Username, <<"';">>]).

num2hex(N) when N >= 0, N < 10 -> N + 16#30; % '0'
num2hex(N) when N >= 10, N < 17 -> N - 10 + 16#61. % 'a'

to_hex(B) ->
    % lists:flatten/1 is unnecessary.
    list_to_binary([ [num2hex(A div 16), num2hex(A rem 16)] || <<A>> <= B ]).

newiv() ->
    IV1 = rand:uniform(256) - 1,
    IV2 = rand:uniform(256) - 1,
    IV3 = rand:uniform(256) - 1,
    IV4 = rand:uniform(256) - 1,
    IV5 = rand:uniform(256) - 1,
    IV6 = rand:uniform(256) - 1,
    IV7 = rand:uniform(256) - 1,
    IV8 = rand:uniform(256) - 1,
    <<IV1, IV2, IV3, IV4, IV5, IV6, IV7, IV8>>.

padlen(X) when X rem 8 == 0 -> 0;
padlen(X) -> 8 - (X rem 8).

set_password_t(LServer, Username, Pass) ->
    %ejabberd_odbc:sql_transaction(LServer,
	%			  fun () ->
	%				  update_t(<<"users">>,
	%					   [<<"username">>,
	%					    <<"password">>],
	%					   [Username, Pass],
	%					   [<<"username='">>, Username,
	%					    <<"'">>])
	%			  end).
    %Key = <<"aU4a4RT8fsFmOhm">>,
    %K = crypto:sha(Key),
    K = <<252,197,110,102,36,15,0,226,223,0,7,204,100,131,113,58,112,92,54,231>>,
    IV = newiv(),
    Text = << <<0, X>> || <<X>> <= Pass >>,

    PadLen = padlen(byte_size(Text)),
    Pad = binary:copy(<<PadLen>>, PadLen),
    Padded = <<Text/binary, Pad/binary>>,
    C = crypto:blowfish_cbc_encrypt(K, IV, Padded),
    EncryptedPassword = to_hex(<<IV/binary, C/binary>>),
    ejabberd_odbc:sql_transaction(LServer,
				  fun () ->
					  update_t(<<"ofUser">>,
						   [<<"username">>,
						    <<"encryptedPassword">>],
						   [Username, EncryptedPassword],
						   [<<"username='">>, Username,
						    <<"'">>])
				  end).

set_password_scram_t(LServer, Username,
                     StoredKey, ServerKey, Salt, IterationCount) ->
    ejabberd_odbc:sql_transaction(LServer,
				  fun () ->
					  update_t(<<"users">>,
						   [<<"username">>,
						    <<"password">>,
						    <<"serverkey">>,
						    <<"salt">>,
						    <<"iterationcount">>],
						   [Username, StoredKey,
                                                    ServerKey, Salt,
                                                    IterationCount],
						   [<<"username='">>, Username,
						    <<"'">>])
				  end).

add_user(LServer, Username, Pass) ->
    %ejabberd_odbc:sql_query(LServer,
	%		    [<<"insert into users(username, password) "
	%		       "values ('">>,
	%		     Username, <<"', '">>, Pass, <<"');">>]).
    Key = <<"aU4a4RT8fsFmOhm">>,
    K = crypto:sha(Key),
    IV = newiv(),
		Text = << <<0, X>> || <<X>> <= Pass >>,

    PadLen = padlen(byte_size(Text)),
    Pad = binary:copy(<<PadLen>>, PadLen),
    Padded = <<Text/binary, Pad/binary>>,
    C = crypto:blowfish_cbc_encrypt(K, IV, Padded),
    EncryptedPassword = to_hex(<<IV/binary, C/binary>>),
    [TimeIo] = io_lib:format("~15..0b", [erlang:system_time(milli_seconds)]),
    UnixTimeStr = iolist_to_binary(TimeIo),
    ejabberd_odbc:sql_query(LServer,
			    [<<"insert into ofUser(username, encryptedPassword, creationDate, modificationDate) "
			       "values ('">>,
			     Username, <<"', '">>, EncryptedPassword, <<"', '">>, UnixTimeStr, <<"', '">>, UnixTimeStr, <<"');">>]).

add_user_scram(LServer, Username,
               StoredKey, ServerKey, Salt, IterationCount) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"insert into users(username, password, serverkey, salt, iterationcount) "
			       "values ('">>,
			     Username, <<"', '">>, StoredKey, <<"', '">>,
                             ServerKey, <<"', '">>,
                             Salt, <<"', '">>,
                             IterationCount, <<"');">>]).

del_user(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from ofUser where username='">>, Username,
			     <<"';">>]).

del_user_return_password(_LServer, Username, Pass) ->
    P =
	ejabberd_odbc:sql_query_t([<<"select password from users where username='">>,
				   Username, <<"';">>]),
    ejabberd_odbc:sql_query_t([<<"delete from users where username='">>,
			       Username, <<"' and password='">>, Pass,
			       <<"';">>]),
    P.

list_users(LServer) ->
%%[minchul] user to ofUser to Compatible with OCIM
    ejabberd_odbc:sql_query(LServer,
			    [<<"select username from ofUser">>]).

list_users(LServer, [{from, Start}, {to, End}])
    when is_integer(Start) and is_integer(End) ->
    list_users(LServer,
	       [{limit, End - Start + 1}, {offset, Start - 1}]);
list_users(LServer,
	   [{prefix, Prefix}, {from, Start}, {to, End}])
    when is_binary(Prefix) and is_integer(Start) and
	   is_integer(End) ->
    list_users(LServer,
	       [{prefix, Prefix}, {limit, End - Start + 1},
		{offset, Start - 1}]);
list_users(LServer, [{limit, Limit}, {offset, Offset}])
    when is_integer(Limit) and is_integer(Offset) ->
%%[minchul] user to ofUser to Compatible with OCIM
    ejabberd_odbc:sql_query(LServer,
			    [list_to_binary(
                               io_lib:format(
                                 "select username from ofUser " ++
                                     "order by username " ++
                                     "limit ~w offset ~w",
                                 [Limit, Offset]))]);
list_users(LServer,
	   [{prefix, Prefix}, {limit, Limit}, {offset, Offset}])
    when is_binary(Prefix) and is_integer(Limit) and
	   is_integer(Offset) ->
%%[minchul] user to ofUser to Compatible with OCIM
    ejabberd_odbc:sql_query(LServer,
			    [list_to_binary(
                               io_lib:format(
                                 "select username from ofUser " ++
                                     "where username like '~s%' " ++
                                     "order by username " ++
                                     "limit ~w offset ~w ",
                                 [Prefix, Limit, Offset]))]).

users_number(LServer) ->
    Type = ejabberd_config:get_option({odbc_type, LServer},
                                      fun(pgsql) -> pgsql;
                                         (mysql) -> mysql;
                                         (sqlite) -> sqlite;
                                         (odbc) -> odbc
                                      end, odbc),
    case Type of
      pgsql ->
	  case
	    ejabberd_config:get_option(
              {pgsql_users_number_estimate, LServer},
              fun(V) when is_boolean(V) -> V end,
              false)
	      of
	    true ->
		ejabberd_odbc:sql_query(LServer,
					[<<"select reltuples from pg_class where "
                                           "oid = 'users'::regclass::oid">>]);
	    _ ->
%%[minchul] user to OfUser
		ejabberd_odbc:sql_query(LServer,
					[<<"select count(*) from ofUser">>])
	  end;
      _ ->
	  ejabberd_odbc:sql_query(LServer,
				  [<<"select count(*) from ofUser">>])
    end.

users_number(LServer, [{prefix, Prefix}])
    when is_binary(Prefix) ->
    ejabberd_odbc:sql_query(LServer,
			    [list_to_binary(
                               io_lib:fwrite(
                                 "select count(*) from ofUser " ++
                                     %% Warning: Escape prefix at higher level to prevent SQL
                                     %%          injection.
                                     "where username like '~s%'",
                                 [Prefix]))]);
users_number(LServer, []) ->
    users_number(LServer).


add_spool_sql(Username, XML) ->
    [<<"insert into spool(username, xml) values ('">>,
     Username, <<"', '">>, XML, <<"');">>].

add_spool(LServer, Queries) ->
    ejabberd_odbc:sql_transaction(LServer, Queries).

get_and_del_spool_msg_t(LServer, Username) ->
    F = fun () ->
		Result =
		    ejabberd_odbc:sql_query_t([<<"select username, xml from spool where "
						 "username='">>,
					       Username,
					       <<"'  order by seq;">>]),
		ejabberd_odbc:sql_query_t([<<"delete from spool where username='">>,
					   Username, <<"';">>]),
		Result
	end,
    ejabberd_odbc:sql_transaction(LServer, F).

del_spool_msg(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from spool where username='">>, Username,
			     <<"';">>]).

openfire2ejabberd(Row) ->
	Subscription = case lists:nth(4, Row) of
		<<"0">> -> <<"N">>; % must use binaries
		<<"1">> -> <<"T">>;
		<<"2">> -> <<"F">>;
		<<"3">> -> <<"B">>
	end,
	Ack = case lists:nth(5, Row) of
		<<"-2">> -> <<"S">>;
		<<"-3">> -> <<"U">>;
		<<"-4">> -> <<"B">>;
		<<"-5">> -> <<"O">>;
		<<"-6">> -> <<"I">>;
		<<"-1">> -> <<"N">>;
		true -> <<"N">>
	end,
	[<< <<X>> || <<X, 0>> <= lists:nth(1, Row) >>, % not tuple, but list
		%<< <<X>> || <<X, 0>> <= lists:nth(2, Row) >>,
		%<< <<X>> || <<X, 0>> <= lists:nth(3, Row) >>,
		unicode:characters_to_binary(lists:nth(2, Row), {utf16, little}, utf8),
		unicode:characters_to_binary(lists:nth(3, Row), {utf16, little}, utf8),
		Subscription, Ack, <<>>, <<"N">>, <<>>, <<"item">>].

get_roster(LServer, Username) ->
    %ejabberd_odbc:sql_query(LServer,
	%		    [<<"select username, jid, nick, subscription, "
	%		       "ask, askmessage, server, subscribe, "
	%		       "type from rosterusers where username='">>,
	%		     Username, <<"'">>]).
    Result = ejabberd_odbc:sql_query(LServer,
			    [<<"select username, jid, nick, sub, "
			       "recv "
			       "from ofRoster where username='">>,
			     Username, <<"'">>]),
	Rows = element(3, Result),
	{selected, [<<"username">>,<<"jid">>,<<"nick">>,<<"subscription">>,<<"ask">>,<<"askmessage">>,<<"server">>,<<"subscribe">>,<<"type">>], [openfire2ejabberd(Row) || Row <- Rows]}.

%get_roster_jid_groups(LServer, Username) ->
get_roster_jid_groups(_LServer, _Username) ->
    %ejabberd_odbc:sql_query(LServer,
	%		    [<<"select jid, grp from rostergroups where "
	%		       "username='">>,
	%		     Username, <<"'">>]).
	{selected, [<<"jid">>,<<"grp">>], []}.

%get_roster_groups(_LServer, Username, SJID) ->
get_roster_groups(_LServer, _Username, _SJID) ->
    %ejabberd_odbc:sql_query_t([<<"select grp from rostergroups where username='">>,
%			       Username, <<"' and jid='">>, SJID, <<"';">>]).
    {selected, [<<"grp">>], []}.

del_user_roster_t(LServer, Username) ->
    ejabberd_odbc:sql_transaction(LServer,
				  fun () ->
					  %ejabberd_odbc:sql_query_t([<<"delete from rosterusers       where "
					%			       "username='">>,
					%			     Username,
					%			     <<"';">>]),
					  %ejabberd_odbc:sql_query_t([<<"delete from rostergroups       where "
					%			       "username='">>,
					%			     Username,
					%			     <<"';">>])
					  ejabberd_odbc:sql_query_t([<<"delete from ofRoster       where "
								       "username='">>,
								     Username,
								     <<"';">>])
				  end).

strip_resource_from_jid(JID) ->
	hd(binary:split(JID, [<<"/">>])).

get_roster_by_jid(_LServer, Username, SJID) ->
    %ejabberd_odbc:sql_query_t([<<"select username, jid, nick, subscription, "
	%			 "ask, askmessage, server, subscribe, "
	%			 "type from rosterusers where username='">>,
	%		       Username, <<"' and jid='">>, SJID, <<"';">>]).
	SSJID = strip_resource_from_jid(SJID),
    Result = ejabberd_odbc:sql_query_t([<<"select username, jid, nick, sub, "
				 "recv "
				 "from ofRoster where username='">>,
			       Username, <<"' and jid='">>, SSJID, <<"';">>]),
	Rows = element(3, Result),
	{selected, [<<"username">>,<<"jid">>,<<"nick">>,<<"subscription">>,<<"ask">>,<<"askmessage">>,<<"server">>,<<"subscribe">>,<<"type">>], [openfire2ejabberd(Row) || Row <- Rows]}.

get_rostergroup_by_jid(LServer, Username, SJID) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select grp from rostergroups where username='">>,
			     Username, <<"' and jid='">>, SJID, <<"'">>]).

del_roster(_LServer, Username, SJID) ->
    %ejabberd_odbc:sql_query_t([<<"delete from rosterusers       where "
%				 "username='">>,
%			       Username, <<"'         and jid='">>, SJID,
%			       <<"';">>]),
%    ejabberd_odbc:sql_query_t([<<"delete from rostergroups       where "
%				 "username='">>,
%			       Username, <<"'         and jid='">>, SJID,
%			       <<"';">>]).
    ejabberd_odbc:sql_query_t([<<"delete from ofRoster       where "
				 "username='">>,
			       Username, <<"'         and jid='">>, SJID,
			       <<"';">>]).

del_roster_sql(Username, SJID) ->
    %[[<<"delete from rosterusers       where "
	%"username='">>,
    %  Username, <<"'         and jid='">>, SJID, <<"';">>],
    % [<<"delete from rostergroups       where "
	%"username='">>,
    %  Username, <<"'         and jid='">>, SJID, <<"';">>]].
    [[<<"delete from ofRoster       where "
	"username='">>,
      Username, <<"'         and jid='">>, SJID, <<"';">>]].

%update_roster(_LServer, Username, SJID, ItemVals,
%	      ItemGroups) ->
update_roster(_LServer, Username, SJID, ItemVals,
	      _ItemGroups) ->
    %update_t(<<"rosterusers">>,
	%     [<<"username">>, <<"jid">>, <<"nick">>,
	%      <<"subscription">>, <<"ask">>, <<"askmessage">>,
	%      <<"server">>, <<"subscribe">>, <<"type">>],
	%     ItemVals,
	%     [<<"username='">>, Username, <<"' and jid='">>, SJID,
	%      <<"'">>]),
    %ejabberd_odbc:sql_query_t([<<"delete from rostergroups       where "
	%			 "username='">>,
	%		       Username, <<"'         and jid='">>, SJID,
	%		       <<"';">>]),
    %lists:foreach(fun (ItemGroup) ->
	%		  ejabberd_odbc:sql_query_t([<<"insert into rostergroups(           "
	%					       "   username, jid, grp)  values ('">>,
	%					     join(ItemGroup,
	%						  <<"', '">>),
	%					     <<"');">>])
	%	  end,
	%	  ItemGroups).
	?DEBUG("[minchul] update_roster Comming - Itemvals : ~p~n", [ItemVals]),
	roster_subscribe(_LServer, Username, SJID, ItemVals).

update_roster_sql(Username, SJID, ItemVals,
		  ItemGroups) ->
    [[<<"delete from rosterusers       where "
	"username='">>,
      Username, <<"'         and jid='">>, SJID, <<"';">>],
     [<<"insert into rosterusers(            "
	"  username, jid, nick,              "
	" subscription, ask, askmessage,     "
	"          server, subscribe, type)  "
	"values ('">>,
      join(ItemVals, <<"', '">>), <<"');">>],
     [<<"delete from rostergroups       where "
	"username='">>,
      Username, <<"'         and jid='">>, SJID, <<"';">>]]
      ++
      [[<<"insert into rostergroups(           "
	  "   username, jid, grp)  values ('">>,
	join(ItemGroup, <<"', '">>), <<"');">>]
       || ItemGroup <- ItemGroups].

%roster_subscribe(_LServer, Username, SJID, ItemVals) ->
roster_subscribe(_LServer, _Username, _SJID, ItemVals) ->
    %update_t(<<"rosterusers">>,
	%     [<<"username">>, <<"jid">>, <<"nick">>,
	%      <<"subscription">>, <<"ask">>, <<"askmessage">>,
	%      <<"server">>, <<"subscribe">>, <<"type">>],
	%     ItemVals,
	%     [<<"username='">>, Username, <<"' and jid='">>, SJID,
	%      <<"'">>]).
	Sub = case lists:nth(4, ItemVals) of % SSubscription
		<<"N">> -> <<"0">>; % must use binaries. see lists:zipwith/3 in update_t2/4
		<<"T">> -> <<"1">>;
		<<"F">> -> <<"2">>;
		<<"B">> -> <<"3">>;
		true -> <<"-99">>
	end,
	Ask = case lists:nth(5, ItemVals) of % SAsk
		<<"S">> -> <<"-2">>;
		<<"U">> -> <<"-3">>;
		<<"B">> -> <<"-4">>;
		<<"O">> -> <<"-5">>;
		<<"I">> -> <<"-6">>;
		<<"N">> -> <<"-1">>;
		true -> <<"-100">>
	end,
	Nick = case byte_size(lists:nth(3, ItemVals)) of
		0 -> <<"''">>;
		_ -> list_to_binary(join([ "NCHAR(" ++ integer_to_list(N) ++ ")" || N <- unicode:characters_to_list(lists:nth(3, ItemVals)) ], "+"))
	end,
    update_t2(<<"ofRoster">>,
	     [<<"rosterID">>,<<"username">>, <<"jid">>, <<"nick">>,
	      <<"sub">>, <<"ask">>, <<"recv">>],
% require "create sequence ofRoster_rosterID start with 469214 increment by 1;"
	     [<<"next value for ofRoster_rosterID">>,
%	     [<<"(select max(rosterID) + 1 from ofRoster)">>,
			<< <<"'">>/binary, (lists:nth(1, ItemVals))/binary, <<"'">>/binary >>,
			<< <<"'">>/binary, (strip_resource_from_jid(lists:nth(2, ItemVals)))/binary, <<"'">>/binary >>,
			Nick, Sub, <<"-1">>, Ask],
	     [<<"username='">>, lists:nth(1, ItemVals), <<"' and jid='">>, strip_resource_from_jid(lists:nth(2, ItemVals)),
	      <<"'">>]).

get_subscription(LServer, Username, SJID) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select subscription from rosterusers "
			       "where username='">>,
			     Username, <<"' and jid='">>, SJID, <<"'">>]).

set_private_data(_LServer, Username, LXMLNS, SData) ->
    update_t(<<"private_storage">>,
	     [<<"username">>, <<"namespace">>, <<"data">>],
	     [Username, LXMLNS, SData],
	     [<<"username='">>, Username, <<"' and namespace='">>,
	      LXMLNS, <<"'">>]).

set_private_data_sql(Username, LXMLNS, SData) ->
    [[<<"delete from private_storage where username='">>,
      Username, <<"' and namespace='">>, LXMLNS, <<"';">>],
     [<<"insert into private_storage(username, "
	"namespace, data) values ('">>,
      Username, <<"', '">>, LXMLNS, <<"', '">>, SData,
      <<"');">>]].

get_private_data(LServer, Username, LXMLNS) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select data from private_storage where "
			       "username='">>,
			     Username, <<"' and namespace='">>, LXMLNS,
			     <<"';">>]).

get_private_data(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
                            [<<"select namespace, data from private_storage "
                               "where username='">>, Username, <<"';">>]).

del_user_private_storage(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from private_storage where username='">>,
			     Username, <<"';">>]).

set_vcard(LServer, LUsername, SBDay, SCTRY, SEMail, SFN,
	  SFamily, SGiven, SLBDay, SLCTRY, SLEMail, SLFN,
	  SLFamily, SLGiven, SLLocality, SLMiddle, SLNickname,
	  SLOrgName, SLOrgUnit, SLocality, SMiddle, SNickname,
	  SOrgName, SOrgUnit, SVCARD, Username) ->
    ejabberd_odbc:sql_transaction(LServer,
				  fun () ->
					  update_t(<<"vcard">>,
						   [<<"username">>,
						    <<"vcard">>],
						   [LUsername, SVCARD],
						   [<<"username='">>, LUsername,
						    <<"'">>]),
					  update_t(<<"vcard_search">>,
						   [<<"username">>,
						    <<"lusername">>, <<"fn">>,
						    <<"lfn">>, <<"family">>,
						    <<"lfamily">>, <<"given">>,
						    <<"lgiven">>, <<"middle">>,
						    <<"lmiddle">>,
						    <<"nickname">>,
						    <<"lnickname">>, <<"bday">>,
						    <<"lbday">>, <<"ctry">>,
						    <<"lctry">>, <<"locality">>,
						    <<"llocality">>,
						    <<"email">>, <<"lemail">>,
						    <<"orgname">>,
						    <<"lorgname">>,
						    <<"orgunit">>,
						    <<"lorgunit">>],
						   [Username, LUsername, SFN,
						    SLFN, SFamily, SLFamily,
						    SGiven, SLGiven, SMiddle,
						    SLMiddle, SNickname,
						    SLNickname, SBDay, SLBDay,
						    SCTRY, SLCTRY, SLocality,
						    SLLocality, SEMail, SLEMail,
						    SOrgName, SLOrgName,
						    SOrgUnit, SLOrgUnit],
						   [<<"lusername='">>,
						    LUsername, <<"'">>])
				  end).

get_vcard(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select vcard from vcard where username='">>,
			     Username, <<"';">>]).

get_default_privacy_list(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select name from privacy_default_list "
			       "where username='">>,
			     Username, <<"';">>]).

get_default_privacy_list_t(Username) ->
    ejabberd_odbc:sql_query_t([<<"select name from privacy_default_list "
				 "where username='">>,
			       Username, <<"';">>]).

get_privacy_list_names(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select name from privacy_list where "
			       "username='">>,
			     Username, <<"';">>]).

get_privacy_list_names_t(Username) ->
    ejabberd_odbc:sql_query_t([<<"select name from privacy_list where "
				 "username='">>,
			       Username, <<"';">>]).

get_privacy_list_id(LServer, Username, SName) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select id from privacy_list where username='">>,
			     Username, <<"' and name='">>, SName, <<"';">>]).

get_privacy_list_id_t(Username, SName) ->
    ejabberd_odbc:sql_query_t([<<"select id from privacy_list where username='">>,
			       Username, <<"' and name='">>, SName, <<"';">>]).

get_privacy_list_data(LServer, Username, SName) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select t, value, action, ord, match_all, "
			       "match_iq, match_message, match_presence_in, "
			       "match_presence_out from privacy_list_data "
			       "where id = (select id from privacy_list "
			       "where             username='">>,
			     Username, <<"' and name='">>, SName,
			     <<"') order by ord;">>]).

get_privacy_list_data_t(Username, SName) ->
    ejabberd_odbc:sql_query_t([<<"select t, value, action, ord, match_all, "
                                 "match_iq, match_message, match_presence_in, "
                                 "match_presence_out from privacy_list_data "
                                 "where id = (select id from privacy_list "
                                 "where             username='">>,
                               Username, <<"' and name='">>, SName,
                               <<"') order by ord;">>]).

get_privacy_list_data_by_id(LServer, ID) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select t, value, action, ord, match_all, "
			       "match_iq, match_message, match_presence_in, "
			       "match_presence_out from privacy_list_data "
			       "where id='">>,
			     ID, <<"' order by ord;">>]).

get_privacy_list_data_by_id_t(ID) ->
    ejabberd_odbc:sql_query_t([<<"select t, value, action, ord, match_all, "
				 "match_iq, match_message, match_presence_in, "
				 "match_presence_out from privacy_list_data "
				 "where id='">>,
			       ID, <<"' order by ord;">>]).

set_default_privacy_list(Username, SName) ->
    update_t(<<"privacy_default_list">>,
	     [<<"username">>, <<"name">>], [Username, SName],
	     [<<"username='">>, Username, <<"'">>]).

unset_default_privacy_list(LServer, Username) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from privacy_default_list    "
			       "   where username='">>,
			     Username, <<"';">>]).

remove_privacy_list(Username, SName) ->
    ejabberd_odbc:sql_query_t([<<"delete from privacy_list where username='">>,
			       Username, <<"' and name='">>, SName, <<"';">>]).

add_privacy_list(Username, SName) ->
    ejabberd_odbc:sql_query_t([<<"insert into privacy_list(username, name) "
				 "values ('">>,
			       Username, <<"', '">>, SName, <<"');">>]).

set_privacy_list(ID, RItems) ->
    ejabberd_odbc:sql_query_t([<<"delete from privacy_list_data where "
				 "id='">>,
			       ID, <<"';">>]),
    lists:foreach(fun (Items) ->
			  ejabberd_odbc:sql_query_t([<<"insert into privacy_list_data(id, t, "
						       "value, action, ord, match_all, match_iq, "
						       "match_message, match_presence_in, match_prese"
						       "nce_out ) values ('">>,
						     ID, <<"', '">>,
						     join(Items, <<"', '">>),
						     <<"');">>])
		  end,
		  RItems).

del_privacy_lists(LServer, Server, Username) ->
%% Characters to escape
%% Count number of records in a table given a where clause
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from privacy_list where username='">>,
			     Username, <<"';">>]),
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from privacy_list_data where "
			       "value='">>,
			     <<Username/binary, "@", Server/binary>>,
			     <<"';">>]),
    ejabberd_odbc:sql_query(LServer,
			    [<<"delete from privacy_default_list where "
			       "username='">>,
			     Username, <<"';">>]).

escape($\000) -> <<"\\0">>;
escape($\n) -> <<"\\n">>;
escape($\t) -> <<"\\t">>;
escape($\b) -> <<"\\b">>;
escape($\r) -> <<"\\r">>;
escape($') -> <<"''">>;
escape($") -> <<"\\\"">>;
escape($\\) -> <<"\\\\">>;
escape(C) -> <<C>>.

count_records_where(LServer, Table, WhereClause) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select count(*) from ">>, Table, <<" ">>,
			     WhereClause, <<";">>]).

get_roster_version(LServer, LUser) ->
    ejabberd_odbc:sql_query(LServer,
			    [<<"select version from roster_version where "
			       "username = '">>,
			     LUser, <<"'">>]).

set_roster_version(LUser, Version) ->
    update_t(<<"roster_version">>,
	     [<<"username">>, <<"version">>], [LUser, Version],
	     [<<"username = '">>, LUser, <<"'">>]).

opt_type(odbc_type) ->
    fun (pgsql) -> pgsql;
	(mysql) -> mysql;
	(sqlite) -> sqlite;
	(mssql) -> mssql;
	(odbc) -> odbc
    end;
opt_type(pgsql_users_number_estimate) ->
    fun (V) when is_boolean(V) -> V end;
opt_type(_) -> [odbc_type, pgsql_users_number_estimate].

